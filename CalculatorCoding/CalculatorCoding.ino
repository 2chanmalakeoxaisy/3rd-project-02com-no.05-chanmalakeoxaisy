#include <Keypad.h>
#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd(0x27, 16, 2);

const byte ROWS = 4; 
const byte COLS = 4; 

char keys[ROWS][COLS] = {
  {'1','2','3','A'},
  {'4','5','6','B'},
  {'7','8','9','C'},
  {'*','0','#','D'}
};


byte rowPins[ROWS] = {9, 8, 7, 6}; 
byte colPins[COLS] = {5, 4, 3, 2}; 


Keypad keypad = Keypad(makeKeymap(keys), rowPins, colPins, ROWS, COLS);

int buzzer = 10;
String inputString = "";
uint32_t numArray[16];
char operatorChar = ' ';
int position = 0;

void setup() {
  Serial.begin(9600);
  pinMode(buzzer, OUTPUT);
 
  lcd.init();
  lcd.backlight();
  clearText();
}

void loop() {
  char key = keypad.getKey();
  
  if (key) {
    tone(buzzer, 2000, 100);
    
    if (key >= '0' && key <= '9') {
      inputString += key;
      numArray[position] = inputString.toInt();
      lcd.print(key);
      
    } else if (key == 'A') {
      operatorChar = '+';
      lcd.print("+");
      position++;
      inputString = "";
      
    } else if (key == 'B') {
      operatorChar = '*';
      lcd.print("*");
      position++;
      inputString = "";
      
    } else if (key == '*') {
      uint32_t total = 0;
      if (operatorChar == '+') {
      	for (int i=0; i<=position; i++){
          total += numArray[i];
        }
        clearText();
        lcd.print('=');
        lcd.print(total);
        
      } else if (operatorChar == '*') {
        total = 1;
      	for (int i=0; i<=position; i++){
          total *= numArray[i];
        }
        clearText();
        lcd.print('=');
        lcd.print(total);
      }
      
    } else if (key == '#') {
      reset();
    }
    
    Serial.println(position);
    for(int i=0; i<=position; i++){
      Serial.print(i);
      Serial.print(" : ");
      Serial.println(numArray[i]);
    }
    Serial.println();
  }
}

void reset(){
  for (int i=0; i<position; i++){
  	numArray[position] = '\0';
  }
  position = 0;
  operatorChar = ' ';
  inputString = "";
  clearText();
}

void clearText(){
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Calculator");
  lcd.setCursor(0, 1);
}